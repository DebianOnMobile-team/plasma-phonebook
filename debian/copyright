Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: plasma-phonebook
Upstream-Contact: plasma-devel@kde.org
Source: https://invent.kde.org/plasma-mobile/plasma-phonebook

Files: *
Copyright: 2015-2023 KDE Community
License: GPL-3

Files: .gitlab-ci.yml
       .kde-ci.yml
       org.kde.phonebook.metainfo.xml
Copyright: None
License: CC0-1.0

Files: debian/*
Copyright: 2023 Marco Mattiolo <marco.mattiolo@hotmail.it>
License: GPL-3

Files: src/contents/ui/AddContactPage.qml
       src/contactcontroller.*
Copyright: 2019-2021 Nicolas Fella <nicolas.fella@gmx.de>
License: LGPL-2+

Files: src/contents/ui/components/ActionButton.qml
       src/contents/ui/components/BottomDrawer.qml
       src/contents/ui/components/DoubleActionButton.qml
Copyright: 2023 Mathis Brüchert <mbb@kaidan.im>
License: GPL-2 OR GPL-3 OR LicenseRef-KDE-Accepted-GPL

Files: src/contents/ui/ContactListItem.qml
       src/contents/ui/RoundImage.qml
Copyright: 2017-2019 Kaidan Developers and Contributors
           2019 Jonah Brüchert <jbb@kaidan.im>
License: GPL-2 OR GPL-3 OR LicenseRef-KDE-Accepted-GPL

Files: src/contents/ui/ContactsPage.qml
Copyright: 2015 Martin Klapetek <mklapetek@kde.org>
           2019 Linus Jahn <lnj@kaidan.im>
           2019 Jonah Brüchert <jbb@kaidan.im>
License: GPL-2 OR GPL-3 OR LicenseRef-KDE-Accepted-GPL

Files: src/contents/ui/DetailPage.qml
Copyright: 2018 Fabian Riethmayer
           2021 Nicolas Fella <nicolas.fella@gmx.de>
License: LGPL-2+

Files: src/contents/ui/Header.qml
Copyright: 2019 Fabian Riethmayer <fabian@web2.0-apps.de>
License: GPL-2 OR GPL-3 OR LicenseRef-KDE-Accepted-GPL

Files: src/contents/ui/main.qml
Copyright: 2019 Linus Jahn <lnj@kaidan.im>
           2019 Jonah Brüchert <jbb@kaidan.im>
License: GPL-2 OR GPL-3 OR LicenseRef-KDE-Accepted-GPL

Files: src/main.cpp
Copyright: 2019 Linus Jahn <lnj@kaidan.im>
License: GPL-2 OR GPL-3 OR LicenseRef-KDE-Accepted-GPL

Files: src/contactimporter.*
Copyright: 2019 Jonah Brüchert <jbb@kaidan.im>
License: GPL-2 OR GPL-3 OR LicenseRef-KDE-Accepted-GPL

Files: src/kpeopleactionplugin/*
Copyright: 2019 Bhushan Shah <bshah@kde.org>
License: GPL-3+

Files: po/*
Copyright: 2019 pan93412 <pan93412@gmail.com>
           2019-2023 Freek de Kruijf <freekdekruijf@kde.nl>
           2019, 2021-2023 Víctor Rodrigo Córdoba <vrcordoba@gmail.com>
           2019, 2021, 2023 Stefan Asserhäll <stefan.asserhall@bredband.net>
           2019, 2022-2023 Rroman Paholik <wizzardsk@gmail.com>
           2020 Marek Laane <qiilaq69@gmail.com>
           2020 Martin Schlander <mschlander@opensuse.org>
           2020-2021 Burkhard Lück <lueck@hube-lueck.de>
           2020-2021 Matej Mrenica <matejm98mthw@gmail.com>
           2020-2023 Sergiu Bivol <sergiu@cip.md>
           2020-2023 Alexander Yavorsky <kekcuha@gmail.com>
           2020-2023 Iñigo Salvador Azurmendi <xalba@ni.eus>
           2020-2023 Karl Ove Hufthammer <karl@huftis.org>
           2020-2023 Luiz Fernando Ranghetti <elchevive@opensuse.org>
           2020-2023 Matjaž Jeran <matjaz.jeran@amis.net>
           2020-2023 Paolo Zamponi <feus73@gmail.com>
           2020-2023 Steve Allewell <steve.allewell@gmail.com>
           2020-2023 Vit Pelcak <vit@pelcak.org>
           2020-2023 Xavier Besnard <xavier.besnard@kde.org>
           2020-2021, 2023 Kristóf Kiszel <kiszel.kristof@gmail.com>
           2020-2021, 2023 Shinjo Park <kde@peremen.name>
           2020-2021, 2023 Tommi Nieminen <translator@legisign.org>
           2021-2023 Kheyyam Gojayev <xxmn77@gmail.com>
           2021-2023 Phu Hung Nguyen <phu.nguyen@kdemail.net>
           2021, 2023 Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
           2021, 2024 A S Alam <aalam.yellow@gmail.com>
           2022-2023 Emir SARI <emir_sari@icloud.com>
           2022-2023 José Nuno Coelho Pires <zepires@gmail.com>
           2022-2023 Sveinn í Felli <sv1@fellsnet.is>
           2022-2023 Temuri Doghonadze <temuri.doghonadze@gmail.com>
           2022-2023 Tyson Tan <tds00@qq.com>
           2023 Adrián Chaves <adrian@chaves.io>
           2023 Enol P. <enolp@softastur.org>
           2023 Frederik Schwarzer <schwarzer@kde.org>
           2023 Giovanni Sora <g.sora@tiscali.it>
           2023 Kisaragi Hiu <mail@kisaragi-hiu.com>
           2023 Oliver Kellogg <okellogg@users.sourceforge.net>
License: GPL-3

Files: po/ca*
       po/uk/*
Copyright: 2019, 2021-2023 Josep M. Ferrer <txemaq@gmail.com>
           2020 Antoni Bella Pérez <antonibella5@yahoo.com>
           2019, 2021-2023 Yuri Chornoivan <yurchor@ukr.net>
License: LGPL-2.1+

License: CC0-1.0
 To the extent possible under law, the author(s) have dedicated all
 copyright and related and neighboring rights to this software to the
 public domain worldwide. This software is distributed without any
 warranty.
 .
 You should have received a copy of the CC0 Public Domain Dedication
 along with this software. If not, see
 <https://creativecommons.org/publicdomain/zero/1.0/>.
 .
 On Debian systems, the complete text of the CC0 Public Domain
 Dedication can be found in `/usr/share/common-licenses/CC0-1.0’.
 
License: GPL-2
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".
 
License: GPL-3
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, version 3.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".
 
License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".
 
License: LGPL-2+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Library General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Library General Public License for more details.
 . 
 You should have received a copy of the GNU Library General Public
 License along with this library; if not, see <http://www.gnu.org/licenses/>.
  
License: LGPL-2.1+
 This package is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in "/usr/share/common-licenses/LGPL-2.1".
 
License: LicenseRef-KDE-Accepted-GPL
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License as
 published by the Free Software Foundation; either version 3 of
 the license or (at your option) at any later version that is
 accepted by the membership of KDE e.V. (or its successor
 approved by the membership of KDE e.V.), which shall act as a
 proxy as defined in Section 14 of version 3 of the license.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
